package Iwona.pl;

import java.util.Scanner;

public class ScannerController {

    private String word;

    private void read() {
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj wyraz: ");
        word = input.nextLine();
    }

    private void checkFirst() {
        read();
       char firstCharacter = word.charAt(0);
        if (Character.isLowerCase(firstCharacter)) {
            word = word.toLowerCase();
        }
        if (Character.isUpperCase(firstCharacter)) {
            word = word.toUpperCase();
        }
    }

    public void print() {
        checkFirst();
        System.out.println(word);
    }
}
